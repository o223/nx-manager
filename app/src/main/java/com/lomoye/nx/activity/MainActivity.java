package com.lomoye.nx.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.lomoye.nx.R;

import util.PermissionsUtil;

public class MainActivity extends BaseActivity implements View.OnClickListener, PermissionsUtil.IPermissionsCallback {

    LinearLayout menuExtractApk, menuApkList;

    PermissionsUtil permissionsUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        //检测权限
        checkAppPermission();
    }

    private void checkAppPermission() {
        permissionsUtil = PermissionsUtil
                .with(this)
                .requestCode(1)
                .isDebug(false)
                .permissions(PermissionsUtil.Permission.Storage.WRITE_EXTERNAL_STORAGE,
                        PermissionsUtil.Permission.Storage.READ_EXTERNAL_STORAGE)
                .request();
    }

    private void initView() {
        initNavBar(false, "工作台", false);

        menuExtractApk = findViewById(R.id.menu_extract_apk);
        menuApkList = findViewById(R.id.menu_apk_list);


        menuExtractApk.setOnClickListener(this);
        menuApkList.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.menu_extract_apk) {
            //提取APK
            Intent intent = new Intent(this, ExtractApkActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.menu_apk_list) {
            //APK清单
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //需要调用onRequestPermissionsResult
        permissionsUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //监听跳转到权限设置界面后再回到应用
        permissionsUtil.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPermissionsGranted(int requestCode, String... permission) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, String... permission) {
        finish();
    }
}