package com.lomoye.nx.activity;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lomoye.nx.R;
import com.lomoye.nx.adatper.InstalledApkListAdapter;
import com.lomoye.nx.model.AppModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

public class ExtractApkActivity extends BaseActivity {

    RecyclerView extractApkRv;

    AlertDialog alertDialog;

    InstalledApkListAdapter apkListAdapter;

    List<AppModel> apps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extract_apk);
        initView();
    }

    private void initView() {
        initNavBar(true, "提取APK", false);
        extractApkRv = findViewById(R.id.extract_apk_rv);

        loadApps();

        apkListAdapter = new InstalledApkListAdapter(this, new ArrayList<>());

        extractApkRv.setLayoutManager(new LinearLayoutManager(this));
        extractApkRv.setAdapter(apkListAdapter);
    }

    private void loadApps() {
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setPadding(0, 10, 0, 40);
        alertDialog = new AlertDialog.Builder(this).setMessage("正在加载").setView(progressBar).create();
        alertDialog.show();

        Executors.newSingleThreadExecutor().execute(this::loadApkData);
    }

    private void loadApkData() {
        PackageManager packageManager = getPackageManager();
        List<PackageInfo> allPackageList = packageManager.getInstalledPackages(0);

        apps = new ArrayList<>();
        for (int i = 0; i < allPackageList.size(); i++) {
            PackageInfo packageInfo = allPackageList.get(i);
            String sourceDir = packageInfo.applicationInfo.sourceDir;
            String packageName = packageInfo.packageName;
            String versionName = packageInfo.versionName;
            int versionCode = packageInfo.versionCode;
            String appName = packageInfo.applicationInfo.loadLabel(packageManager).toString();
            Drawable iconDrawable = packageInfo.applicationInfo.loadIcon(packageManager);

            //系统应用不要
            if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                continue;
            }
            //系统跟新不要
            if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                continue;
            }

            AppModel appModel = new AppModel();
            appModel.name = appName;
            appModel.packageName = packageName;
            appModel.versionName = versionName;
            appModel.versionCode = versionCode;
            appModel.path = sourceDir;
            appModel.icon = iconDrawable;
            appModel.type = 0;

            apps.add(appModel);
        }


        runOnUiThread(() -> {
            apkListAdapter.refreshData(apps);
            alertDialog.dismiss();
        });
    }
}