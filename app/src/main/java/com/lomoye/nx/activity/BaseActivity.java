package com.lomoye.nx.activity;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lomoye.nx.R;


public class BaseActivity extends Activity {

    private ImageView ivBack, ivMe;
    private TextView tvTitle;


    protected void initNavBar(boolean isShowBack, String title, boolean isShowMe) {
        ivBack = findViewById(R.id.iv_back);
        tvTitle = findViewById(R.id.tv_title);
        ivMe = findViewById(R.id.iv_me);

        ivBack.setVisibility(isShowBack ? View.VISIBLE : View.INVISIBLE);
        ivMe.setVisibility(isShowMe ? View.VISIBLE : View.GONE);
        tvTitle.setText(title);

        ivBack.setOnClickListener(v -> onBackPressed());

        ivMe.setOnClickListener(v -> {
//                startActivity(new Intent(this, MeActivity.class));
        });
    }

}
