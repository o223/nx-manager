package com.lomoye.nx.model;

import android.graphics.drawable.Drawable;


/**
 * 已安装的软件包
 */
public class AppModel {

    //软件名
    public String name;

    //包名
    public String packageName;

    //版本号
    public String versionName;

    //版本code
    public int versionCode;

    //安装包路径
    public String path;

    //图标
    public Drawable icon;

    //类型：用户、系统、跟新
    public int type;
}
