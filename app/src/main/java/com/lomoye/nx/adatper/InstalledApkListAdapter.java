package com.lomoye.nx.adatper;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.lomoye.nx.R;
import com.lomoye.nx.model.AppModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;


public class InstalledApkListAdapter extends RecyclerView.Adapter<InstalledApkListAdapter.ViewHolder> {

    private final Context context;

    private final List<AppModel> apps;

    public InstalledApkListAdapter(Context context, List<AppModel> apps) {
        this.context = context;
        this.apps = apps;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.installed_app_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AppModel appModel = apps.get(position);

        holder.icon.setImageDrawable(appModel.icon);
        holder.nameTv.setText(appModel.name);
        holder.versionTv.setText(appModel.versionName);
        holder.packageTv.setText(appModel.packageName);

        holder.itemView.setOnClickListener(v -> {
            showConfirmDialog(appModel);
        });
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView icon;
        TextView nameTv;
        TextView versionTv;
        TextView packageTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            icon = itemView.findViewById(R.id.installed_app_item_iv_icon);
            nameTv = itemView.findViewById(R.id.installed_app_item_tv_name);
            versionTv = itemView.findViewById(R.id.installed_app_item_tv_version);
            packageTv = itemView.findViewById(R.id.installed_app_item_tv_package);
        }
    }

    public void refreshData(List<AppModel> appList) {
        apps.clear();
        apps.addAll(appList);
        notifyDataSetChanged();
    }

    private void showConfirmDialog(AppModel appModel) {
        final AlertDialog.Builder normalDialog =
                new AlertDialog.Builder(context);

        normalDialog.setMessage("确定要提取该软件吗？");
        normalDialog.setPositiveButton("确定",
                (dialog, which) -> {
                    backupApp(appModel);
                });
        normalDialog.setNegativeButton("取消",
                (dialog, which) -> dialog.cancel());
        // 显示
        normalDialog.show();
    }


    private void backupApp(AppModel appModel) {
        String path = appModel.path;
        String name = appModel.name;
        String versionName = appModel.versionName;

        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(context, "sd卡未识别", Toast.LENGTH_SHORT).show();
            return;
        }

        File formFile = new File(path);
        if (!formFile.exists()) {
            Toast.makeText(context, "app路径不存在", Toast.LENGTH_SHORT).show();
            return;
        }
        String appBackupPath = Environment.getExternalStorageDirectory() + File.separator + "NX" + File.separator + "apks" + File.separator;
        String toFileName = name + "_" + versionName + ".apk";
        File toFilePath = new File(appBackupPath);
        if (!toFilePath.exists()) {
            boolean result = toFilePath.mkdirs();
            if (!result) {
                Toast.makeText(context, "创建文件夹失败", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        File toFile = new File(toFilePath, toFileName);
        if (toFile.exists()) {
            boolean result = toFile.delete();
            if (!result) {
                Toast.makeText(context, "删除文件失败", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        try {
            boolean result = toFile.createNewFile();
            if (!result) {
                Toast.makeText(context, "创建文件失败", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        if (!toFile.exists()) {
            Toast.makeText(context, "目标文件生成失败", Toast.LENGTH_SHORT).show();
            return;
        }
        javaNioTransfer(formFile, toFile);
        Toast.makeText(context, "Apk已保存到\n" + toFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
    }

    private void javaNioTransfer(File source, File target) {
        FileChannel in;
        FileChannel out;
        FileInputStream inStream = null;
        FileOutputStream outStream = null;
        try {
            inStream = new FileInputStream(source);
            outStream = new FileOutputStream(target);
            in = inStream.getChannel();
            out = outStream.getChannel();
            in.transferTo(in.position(), in.size(), out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inStream != null) {
                    inStream.close();
                }
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
